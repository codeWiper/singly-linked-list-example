//Singly List List
    function LinkedList(){
        this.len = 0;
        this.head = null;
    }

    function Node(data){
        this.data = data;
        this.next = null;
    }

    LinkedList.prototype.Insert = function(x){
        var curr, prev;
        curr = prev = this.head;
        var status = document.getElementById("status");
        status.textContent = "";
        var node = new Node(x);
        // when list is empty
        if(this.head === null){
            this.head = node;
            this.len++;
            status.textContent = x + " added!";
        }
        // when the input data is less than "head node" data
        else if(x < this.head.data){
            node.next = this.head;
            this.head = node;
            this.len++;
            status.textContent = x + " added!";
        }
        // inserting in the middle of list
        else {
            while(curr !== null){
                if(x > curr.data){
                    prev = curr;
                    curr = curr.next;
                }
                else{
                    node.next = curr;
                    prev.next = node;
                    this.len++;
                    status.textContent = x + " added!";
                    break;
                }
            }
            if(curr === null){
                prev.next = node;
                status.textContent = x + " added!";
                this.len++;
            }
        }
        Print();

    }

    LinkedList.prototype.Delete = function(x){
        var curr,prev;
        curr = prev = this.head;
        window.deleted = 0;
        var status = document.getElementById("status");
        status.textContent = "";
        if(this.len === 0){
            status.textContent = "List is empty!!";
            return;
        }
        else{
            while(curr !== null){
                if(this.head.data === x){
                    this.head = curr.next;
                    curr.next = null;
                    curr = this.head;
                    window.deleted = 1;
                    this.len--;
                    break;
                }
                else if(curr.data === x){
                    prev.next = curr.next;
                    curr.next = null;
                    window.deleted = 1;
                    this.len--;
                    break;
                }
                prev = curr;
                curr = curr.next;
            }
            if(window.deleted === 0){
                status.textContent = x + " Does not exist";
            }
            else{
                status.textContent = x + " succesfully deleted";
            }
    
            Print();
        }
        
    }
    
    LinkedList.prototype.Search = function(x){
        var curr = this.head;
        var status = document.getElementById("status");
        var position = 0;
        status.textContent = "";
        while(curr !== null){
            if(curr.data === x){
                status.textContent = x + " found at position " + (position + 1);
                break;
            }
            else{
                position++;
                curr = curr.next;
            }
        }
        if(curr === null){
            status.textContent = x + " was not found!!";
        }
    }


    LinkedList.prototype.Reverse = function(){
        var status = document.getElementById("status");
        status.textContent = "";
        var head = this.head;
        var curr, ftr, temp;
        curr = head;
        ftr = head.next;
        while(ftr.next !== null){
               temp = ftr.next;
               ftr.next = curr;
               curr = ftr;
               ftr = temp;
        }
        temp = ftr;
        ftr.next = curr;
        head.next = null;
        this.head = temp;
        status.textContent = "List reversed";
        Print();
    }

    function Print(){
        var parent = document.getElementById("data");
        var i = j = 0;
        var curr = list.head;
        if(parent.childNodes.length > 0){
            for(j = 0; j < parent.childNodes.length; ){
                parent.childNodes[j].remove();
            }
        }
        
            while(curr !== null){ 
                let child = document.createElement('div');
                child.setAttribute("id", i.toString());
                child.textContent = curr.data;  
                parent.appendChild(child);  
                curr = curr.next;
                i++;
            }
    }
    var list = new LinkedList();
